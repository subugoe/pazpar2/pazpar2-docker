SUB Pazpar2 images
==================

# Introduction

## Finished

-  Moved GitHub repositories to [GitLab](https://gitlab.gwdg.de/subugoe/pazpar2)

-  Tested communications between containers

-  Finished [Pazpar2 base image](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-docker-base) and documented it 

-  Added a simple testing script

-  Using GitLab CI to build [images](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-docker/container_registry)

-  Created deployment file (`docker-compose.prod.yml`)

-  Documentation (this file)

## TODO

-  Configure Nginx frontend properly
  
## Experimental Features

-  Using Nginx as HTTP Frontend

# Quick start

## Checkout the repository

To avoid the need to have a `git` client in each container this repository contains it's 
dependencies as submodules, you need to check them out first:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone --recurse-submodules -j8 https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-docker.git
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** If `git` complains that `-j` isn't supported, your `git` version is to old. It's completely safe to omit this option.

## Build the images and start containers

You can run the builds and start in one line after you changed into the checkout directory:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh && docker-compose -f docker-compose.yml -f docker-compose-apache.yml up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Technical documentation

## Images

The restructured application is composed of either two or three Docker images. 

### `pazpar2` image

This image contains the content of `./docker/sub-pazpar2/pazpar2-etc` and `./docker/sub-pazpar2/pazpar2-SUB`. See also
the section ['Structure of this repository'](#./docker/sub-pazpar2/pazpar2-etc). These are copied into `PAZPAR2_DIR`, existing 
symbolic links are removed since they cause problems when `pazpar2` tries to load it's configuration.
This image runs as user `pazpar2`.

**Directory:** `./docker/pazpar2`

#### Build process

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t pazpar2-docker_pazpar2 -f docker/pazpar2/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Arguments

No Arguments, see the list of environment variables below.

#### Environment variables

| Name            | Default                          | Description                                                 |
|-----------------|----------------------------------|-------------------------------------------------------------|
| `BUILD_CONTEXT` | /mnt/build-context               | Where the contents of the Docker context will be mounted at |
| `CONF_FILE`     | /opt/pazpar2/pazpar2-SUB/SUB.xml | The configuration file to use                               |
| `PAZPAR2_DIR`   | /opt/pazpar2                     | The directory to copy the configuration to, also `WORKDIR`  |

#### Important paths and files inside the image

| Name                                | Description                     |
|-------------------------------------|---------------------------------|
| `/usr/local/sbin/pazpar2`           | Pazpar2 binary                  |
| `/opt/pazpar2`                      | Directory for the Pazpar2 image |
| `/opt/pazpar2/pazpar2-SUB/SUB.xml`  | SUB configuration file          |
| `/opt/pazpar2/pazpar2-SUB/test.xml` | Test configuration file         |

### `web` image

There are two different possible web frontends, default is the Apache variant. 

#### Apache variant (default)

The Apache variant includes the web server and a PHP interpreter in one image, this mages it easier to maintain but will also 
result in a lager image when compared to the PHP and Nginx image solution. A Apache configuration file 
(`/docker/web/conf/pazpar2.conf`) is provided and will be placed in `/etc/apache2/sites-available`. It will be linked into 
`/etc/apache2/sites-enabled`. The default file (`000-default.conf`) will be deleted.
Depending on the `MODE` either `php.ini-production` or `php.ini-development` will be copied to `php.ini` in `PHP_INI_DIR`.

**Directory:** `./docker/web`

##### Build process

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t pazpar2-docker_web -f docker/web/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

##### Arguments

| Name   | Default             | Description                                                       |
|--------|---------------------|-------------------------------------------------------------------|
| `MODE` | dev                 | If set to `dev` the PHP interpreter will emit more debug messages |
| `MAIL` | webmaster@localhost | The mail address apache will report on errors                     |


##### Environment variables

| Name            | Default            | Description                                                 |
|-----------------|--------------------|-------------------------------------------------------------|
| `BUILD_CONTEXT` | /mnt/build-context | Where the contents of the Docker context will be mounted at |
| `WORKDIR`       | /var/www/html      | The working directory                                       |

##### Important paths and files inside the image

| Name                           | Description                                                                                              |
|--------------------------------|----------------------------------------------------------------------------------------------------------|
| `/usr/local/etc/php/php.ini`   | PHP configuration file, see `PHP_INI_DIR` defined by the extended image                                  |
| `/etc/apache2/sites-available` | Directory for Apache configuration files                                                                 |
| `/etc/apache2/sites-enabled`   | Directory for Apache configuration files to actually use (create link to `/etc/apache2/sites-available`) |
| `/var/www/html`                | Apache `DocumentRoot`                                                                                    |

#### Nginx variant (experimental)

**Note:** This image is experimental!

The Nginx variant is provided by a pre build Nginx Docker image. The required files are part of the `php` image.

##### Configuration

Since for Nginx a pre build image is used, it has to be configured via `docker-comnpose.yml`, the configuration is 
at `/docker/web/conf/site.conf`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  web:
    image: nginx:stable-alpine
    links:
      - php
    volumes:
      - type: bind
        source: ./docker/web/conf/site.conf
        target: /etc/nginx/conf.d/default.conf
      - type: volume
        source: web-app
        target: /var/www/html/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example for `docker-compose.yml`, the second volume definition (`web-app`) is used to share the scripts between the `web` 
and `php` image.

### `php` image

**Note:** This image is experimental, as is the Nginx variant of the `web` image. It's only used by this image.

The `php` image is used to provide the files for the `nginx` web frontend.

**Directory:** `./docker/php`

#### Build process

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t pazpar2-docker_php -f docker/php/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Arguments

No Arguments, see the list of environment variables below.

#### Environment variables

| Name            | Default            | Description                                                 |
|-----------------|--------------------|-------------------------------------------------------------|
| `BUILD_CONTEXT` | /mnt/build-context | Where the contents of the Docker context will be mounted at |
| `WORKDIR`       | /var/www/html      | The working directory                                       |

##### Important paths and files inside the image

| Name                           | Description                                                                                              |
|--------------------------------|----------------------------------------------------------------------------------------------------------|
| `/usr/local/etc/php/php.ini`   | PHP configuration file, see `PHP_INI_DIR` defined by the extended image                                  |
| `/var/www/html`                | Working directory and web server document root                                                           |

## Using `docker-compose`

### Default configuration: Apache

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose-apache.yml up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Experimental configuration: Nginx 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose-nginx.yml up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Different Docker usage

Docker contains a serious design flaw regarding the `COPY` command, see the [issue tracker](https://github.com/moby/moby/issues/15858), 
there is a [solution](https://github.com/moby/moby/issues/39530#issuecomment-511815220) which requires a fairly new Docker (18.09 or newer).

To build these images you need Docker (same as or newer then 18.09) to [allow experimental features](https://docs.docker.com/engine/reference/commandline/dockerd/#description).

You need to run `docker build` with the following prefix to use BuildKit's experimental features:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Be aware, that `docker-compose` doesn't understand how to build with BuildKit yet! [See below](./docs/docker-compose.md#docker-compose)

To get every output of `docker build` commands append the following to the `build` command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--progress=plain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using the test script

There is a simple testing script in the `tests` directory. It requires `wget` to be on `$PATH`.
It will try to query a locally running Pazpar2 with different script names (referred as `PREFIXES`).

### Configuration

| Variable   | Default                                 | Description                                                             |
|------------|-----------------------------------------|-------------------------------------------------------------------------|
| `HOST`     | http://localhost:8008                   | The URL to run the tests against                                        |
| `PREFIXES` | /pazpar2/search.pz2 /pazpar2-access.php | Different pathes (seperated by a space) to prepend to the query strings |

# Prebuild images

Pre build images are available from GitLab for the default frontend variant (Apache) and Pazpar2. You can get a list of 
existing images [there](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-docker/container_registry).

<a name="repository-structure" />

# Structure of this repository

This repository includes the following three GitLab repositories as sub modules. These have been moved from GitHub and 
are still available there in the 'subugoe' organisation: [https://github.com/subugoe/](https://github.com/subugoe/)

| Name           | Repository URL                                                                                                 | Location of the sub module       | Documentation                                                       |
|----------------|----------------------------------------------------------------------------------------------------------------|----------------------------------|---------------------------------------------------------------------|
| pazpar2-access | [https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-access](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-access) | ./docker/php/pazpar2-access      | [Readme.markdown](./docker/php/pazpar2-access/Readme.markdown)      |
| pazpar2-etc    | [https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-etc](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-etc)       | ./docker/sub-pazpar2/pazpar2-etc | [README.markdown](./docker/sub-pazpar2/pazpar2-etc/README.markdown) |
| pazpar2-SUB    | [https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-SUB](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-SUB)       | ./docker/sub-pazpar2/pazpar2-SUB | [README.markdown](./docker/sub-pazpar2/pazpar2-SUB/README.markdown) |

# Deployment / Production

For deploying the SUB Pazpar2 service it's recommended to use the pre build images as [provided by Gitlab](https://gitlab.gwdg.de/subugoe/pazpar2/pazpar2-docker/container_registry). 
And to use the Apache web frontend variant. There is a file `docker-compose.prod.yml` which fullfils both requirements:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.prod.yml up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
