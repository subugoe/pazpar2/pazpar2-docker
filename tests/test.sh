#!/bin/bash

HOST=http://localhost:8008
PREFIXES="/pazpar2/search.pz2 /pazpar2-access.php"

# Commands (defaults)
GREP="/bin/grep"
FIND="/usr/bin/find"
SED="/bin/sed"

# Other Settings
VERBOSE=false

# For debugging and development on OS X 
if [[ "$OSTYPE" == "darwin"* ]]; then
        echo "Mac OS X detected using GNU tool from Mac Ports"
        GREP="/opt/local/bin/ggrep"
        FIND="/opt/local/bin/gfind"
        SED="/opt/local/bin/gsed"
else
        GREP=$(which grep)
        FIND=$(which find)
        SED=$(which sed)
fi

echo "The following programs will be used"
echo "Using 'grep' at $GREP"
echo "Using 'find' at $FIND"
echo "Using 'sed' at $SED"

for PREFIX in $PREFIXES; do
	echo "Running against '$HOST' with prefix '$PREFIX'"

# Info should return 200
	if [ `wget --server-response -q -O - $HOST$PREFIX?command=info 2>&1 | head -1 | awk '{ print $2 }'` -ne 200 ] ; then 
		echo "Server didn't return 200, exiting"
		exit 2
	fi

# This should return 417
	if [ `wget --server-response -q -O - $HOST$PREFIX?command=init 2>&1 | head -1 | awk '{ print $2 }'` -ne 417 ] ; then
		echo "Server didn't return 417, exiting"
		exit 3
	fi

	SESSION=$(wget -q -O - $HOST$PREFIX'?command=init&service=SUB' | tr -d '\n' | gsed "s/.*<session>\(.*\)<\/sess.*/\1/g")
	echo "Got session '$SESSION'"

	wget -O - $HOST$PREFIX"pazpar2/search.pz2?command=stat&session=$SESSION&windowid"
	wget -O - $HOST$PREFIX"?command=search&query=test&session=$SESSION&windowid="
	wget -O - $HOST$PREFIX"?command=show&session=$SESSION&start=0&num=1500&sort=date%3A0&block=1&type=xml&windowid="

done
